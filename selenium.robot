*** Settings ***
Documentation  Resource file with reusable keywords and variables.
...
...            The system specific keywords created here form our own
...            domain specific language.  They utilize keywords provided
...            by the imported Selenium2Library.
Library        SeleniumLibrary

*** Variables ***


*** Keywords ***
Open Safari
  Create WebDriver  Safari

Open Secured Chrome
  ${options}=  Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()  sys, selenium.webdriver
  Call Method  ${options}  add_experimental_option  useAutomationExtension  ${FALSE}
  Call Method  ${options}  add_argument             start-maximized
  Create WebDriver  Chrome  chrome_options=${options}

Page title should be
  [Arguments]      ${title}
  Title Should Be  ${title}

Page should contain
  [Arguments]               ${text}
  Wait Until Page Contains  ${text}

Press Return
  # Press the Return key with focus on the specified element
  [Arguments]  ${pageElementId}
  Press Key    ${pageElementId}  \\13

Open "${engine}" SearchPage
  ${SEARCH_ENGINES}=  Create Dictionary  Google  http://www.google.com  Bing  http://www.bing.com
  ${url}=  Evaluate  $SEARCH_ENGINES.get("${engine}")
  Go To  ${url}
  Page title should be  ${engine}

Submit Search
  [Arguments]       ${searchkey}      ${pageElementId}
  input text        ${pageElementId}  ${searchkey}
  and press return  ${pageElementId}

Google Search
  [Arguments]    ${searchkey}
  submit search  ${searchkey}  id=lst-ib

Bing Search
  [Arguments]    ${searchkey}
  submit search  ${searchkey}  id=sb_form_q


