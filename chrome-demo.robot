*** Settings ***
Documentation  Demo using Chrome to run Selenium Tests
Resource       selenium.robot

Suite Setup     Open Secured Chrome
Suite Teardown  Close All Browsers

*** Test Cases ***
Search Google for devops and find puppet.com
  Given browser is opened to Google Search
  When I submit a Google search for "devops"
  Then the result should contain "en.wikipedia.org/wiki/DevOps"

Search Bing for devops and find puppet.com
  Given browser is opened to Bing Search
  When I submit a Bing search for "devops"
  Then the result should contain "en.wikipedia.org/wiki/DevOps"


*** Keywords ***
browser is opened to Google Search
  Open "Google" SearchPage

browser is opened to Bing Search
  Open "Bing" SearchPage

I submit a Google search for "${searchstring}"
  Google Search  ${searchstring}

I submit a Bing search for "${searchstring}"
  Bing Search  ${searchstring}

the result should contain "${result}"
  Page should contain  ${result}