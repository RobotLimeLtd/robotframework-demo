# Setup


## Pre-requisites
* Python (2.7 will do)
* Chromedriver in your PATH - https://sites.google.com/a/chromium.org/chromedriver/home
* Python modules
  * robotframework
  * robotframework-seleniumlibrary (selenium2library is deprecated)
  

# To Run

    robot -d output chrome-demo.robot
    
or

    robot -d output safari-demo.robot


# IMPORTANT NOTES

## Safari issues
If you get the error:  

    AttributeError: 'WebDriver' object has no attribute 'service'

then you will need to downgrade the selenium module installed by robotframework-seleniumlibrary

    pip uninstall selenium
    pip install selenium==3.9
    
    
## Python Issues with MacOS

It might be easier to use brew to install python (and everything else), but if you'd rather do everything using the
copy of Python that ships with your Mac...

First, install pip into core Python…

    sudo easy_install pip

Then upgrade these two modules

    pip install --upgrade setuptools six —user python

Now, install PipEnv (https://docs.pipenv.org)

    pip install --user pipenv

Because this is a user install, you will need to add the following to ~/.profile

    PYTHON_BIN_PATH="$(python -m site --user-base)/bin"
    PATH=${PATH}:${PYTHON_BIN_PATH}


Now in any development project directory, you use pipenv to install your modules, e.g.

    python -m pipenv install robotframework robotframework-seleniumlibrary

And then, any time  activate the virtual environment with 

    pipenv shell

